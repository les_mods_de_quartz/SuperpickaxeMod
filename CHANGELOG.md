## [1.1.0](https://gitlab.com/les_mods_de_quartz/SuperpickaxeMod/compare/1.0.0...1.1.0) (2022-08-24)


### Features

* Ajout du DarKOre, du gardien du DarKOre, des fragments, fragments solides et lingot de DarKOre ([868b602](https://gitlab.com/les_mods_de_quartz/SuperpickaxeMod/commit/868b6025f7e5dd46d782abd07958708b4edbe96f))
* modification du portail ([da7584a](https://gitlab.com/les_mods_de_quartz/SuperpickaxeMod/commit/da7584a9bfdd19dc3d251a400f700d05bf728ae2))

## [1.0.0](https://gitlab.com/les_mods_de_quartz/SuperpickaxeMod/compare/...1.0.0) (2022-08-16)


### Bug Fixes

* Ajout de la pipeline de construction ([a9cb85e](https://gitlab.com/les_mods_de_quartz/SuperpickaxeMod/commit/a9cb85ea5dfb0dd3e2a3d446c325b3968cfb8c2d))
* Changement de la branche de tagging ([0e09d94](https://gitlab.com/les_mods_de_quartz/SuperpickaxeMod/commit/0e09d9466a75dbc2ad510f7cc50609fceec890e3))


### Features

* ajout de la pipeline de construction ([c9c600b](https://gitlab.com/les_mods_de_quartz/SuperpickaxeMod/commit/c9c600b0c127636e49f6c2c467725178d78f5e2d))
* ajout de la pipeline de construction ([8e9c7e7](https://gitlab.com/les_mods_de_quartz/SuperpickaxeMod/commit/8e9c7e78ed60e12970e5f5752e3d85eb8429e9b5))
* ajout de la pipeline de construction ([3e541b8](https://gitlab.com/les_mods_de_quartz/SuperpickaxeMod/commit/3e541b8081adf8e627d96a6e747a02507b9c5860))
* Ajout de la pipeline de construction ([88b17b4](https://gitlab.com/les_mods_de_quartz/SuperpickaxeMod/commit/88b17b4447159f1d75285d049f79f8b510861d76))
