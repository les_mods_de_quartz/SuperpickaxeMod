
package net.mcreator.mod.item;

import net.minecraft.world.level.Level;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.item.Rarity;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.BucketItem;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.network.chat.Component;

import net.mcreator.mod.init.SuperpickaxemodModTabs;
import net.mcreator.mod.init.SuperpickaxemodModFluids;

import java.util.List;

public class YelloritefondueItem extends BucketItem {
	public YelloritefondueItem() {
		super(SuperpickaxemodModFluids.YELLORITEFONDUE,
				new Item.Properties().craftRemainder(Items.BUCKET).stacksTo(1).rarity(Rarity.RARE).tab(SuperpickaxemodModTabs.TAB_SUPERPICKAXEMOD));
	}

	@Override
	public void appendHoverText(ItemStack itemstack, Level world, List<Component> list, TooltipFlag flag) {
		super.appendHoverText(itemstack, world, list, flag);
		list.add(new TextComponent("chaut bouillant!!"));
	}
}
