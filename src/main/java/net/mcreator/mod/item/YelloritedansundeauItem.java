
package net.mcreator.mod.item;

import net.minecraft.world.item.UseAnim;
import net.minecraft.world.item.Rarity;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Item;

import net.mcreator.mod.init.SuperpickaxemodModTabs;

public class YelloritedansundeauItem extends Item {
	public YelloritedansundeauItem() {
		super(new Item.Properties().tab(SuperpickaxemodModTabs.TAB_SUPERPICKAXEMOD).stacksTo(64).fireResistant().rarity(Rarity.COMMON));
	}

	@Override
	public UseAnim getUseAnimation(ItemStack itemstack) {
		return UseAnim.EAT;
	}

	@Override
	public boolean hasCraftingRemainingItem() {
		return true;
	}

	@Override
	public ItemStack getContainerItem(ItemStack itemstack) {
		return new ItemStack(Items.BUCKET);
	}

	@Override
	public int getUseDuration(ItemStack itemstack) {
		return 32;
	}
}
