
package net.mcreator.mod.item;

import net.minecraft.world.item.Rarity;
import net.minecraft.world.item.Item;

import net.mcreator.mod.init.SuperpickaxemodModTabs;

public class OrcompactItem extends Item {
	public OrcompactItem() {
		super(new Item.Properties().tab(SuperpickaxemodModTabs.TAB_SUPERPICKAXEMOD).stacksTo(64).rarity(Rarity.COMMON));
	}
}
