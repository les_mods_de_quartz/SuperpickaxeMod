
package net.mcreator.mod.item;

import net.minecraft.world.level.Level;
import net.minecraft.world.item.Rarity;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Item;
import net.minecraft.world.entity.Entity;

import net.mcreator.mod.procedures.DarKFragmentQuandLitemEstDansLinventaireParTickProcedure;
import net.mcreator.mod.init.SuperpickaxemodModTabs;

public class DarKFragmentItem extends Item {
	public DarKFragmentItem() {
		super(new Item.Properties().tab(SuperpickaxemodModTabs.TAB_SUPERPICKAXEMOD).stacksTo(64).rarity(Rarity.RARE));
	}

	@Override
	public void inventoryTick(ItemStack itemstack, Level world, Entity entity, int slot, boolean selected) {
		super.inventoryTick(itemstack, world, entity, slot, selected);
		DarKFragmentQuandLitemEstDansLinventaireParTickProcedure.execute(entity, itemstack);
	}
}
