
package net.mcreator.mod.block;

import net.minecraft.world.level.material.Material;
import net.minecraft.world.level.material.FlowingFluid;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.block.LiquidBlock;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.Level;
import net.minecraft.world.entity.Entity;
import net.minecraft.core.BlockPos;

import net.mcreator.mod.procedures.YelloritefondueQuandLeBlocVoisinChangeProcedure;
import net.mcreator.mod.procedures.YelloritefondueLorsqueLeMobjoueurEntreEnCollisionAvecLeBlocProcedure;
import net.mcreator.mod.init.SuperpickaxemodModFluids;

public class YelloritefondueBlock extends LiquidBlock {
	public YelloritefondueBlock() {
		super(() -> (FlowingFluid) SuperpickaxemodModFluids.YELLORITEFONDUE.get(), BlockBehaviour.Properties.of(Material.LAVA).strength(100f)

		);
	}

	@Override
	public void neighborChanged(BlockState blockstate, Level world, BlockPos pos, Block neighborBlock, BlockPos fromPos, boolean moving) {
		super.neighborChanged(blockstate, world, pos, neighborBlock, fromPos, moving);
		YelloritefondueQuandLeBlocVoisinChangeProcedure.execute(world, pos.getX(), pos.getY(), pos.getZ());
	}

	@Override
	public void entityInside(BlockState blockstate, Level world, BlockPos pos, Entity entity) {
		super.entityInside(blockstate, world, pos, entity);
		YelloritefondueLorsqueLeMobjoueurEntreEnCollisionAvecLeBlocProcedure.execute(entity);
	}
}
