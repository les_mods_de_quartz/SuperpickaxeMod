
package net.mcreator.mod.client.renderer;

import net.minecraft.resources.ResourceLocation;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.client.renderer.entity.EntityRendererProvider;

import net.mcreator.mod.entity.GardienduDarKOreEntity;
import net.mcreator.mod.client.model.ModelGardienDuDarKore;

public class GardienduDarKOreRenderer extends MobRenderer<GardienduDarKOreEntity, ModelGardienDuDarKore<GardienduDarKOreEntity>> {
	public GardienduDarKOreRenderer(EntityRendererProvider.Context context) {
		super(context, new ModelGardienDuDarKore(context.bakeLayer(ModelGardienDuDarKore.LAYER_LOCATION)), 0.5f);
	}

	@Override
	public ResourceLocation getTextureLocation(GardienduDarKOreEntity entity) {
		return new ResourceLocation("superpickaxemod:textures/entities/texture_gardiens.png");
	}
}
