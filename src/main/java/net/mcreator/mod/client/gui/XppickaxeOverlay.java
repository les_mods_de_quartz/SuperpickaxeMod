
package net.mcreator.mod.client.gui;

import org.checkerframework.checker.units.qual.h;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.world.level.Level;
import net.minecraft.world.entity.player.Player;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.client.renderer.GameRenderer;
import net.minecraft.client.Minecraft;

import net.mcreator.mod.procedures.ProcAffichageXpPickaxeProcedure;
import net.mcreator.mod.procedures.ProcAffichageUltimeXpProcedure;
import net.mcreator.mod.procedures.ProcAffichageOverlayProcedure;
import net.mcreator.mod.network.SuperpickaxemodModVariables;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.platform.GlStateManager;

@Mod.EventBusSubscriber({Dist.CLIENT})
public class XppickaxeOverlay {
	@SubscribeEvent(priority = EventPriority.NORMAL)
	public static void eventHandler(RenderGameOverlayEvent.Pre event) {
		if (event.getType() == RenderGameOverlayEvent.ElementType.ALL) {
			int w = event.getWindow().getGuiScaledWidth();
			int h = event.getWindow().getGuiScaledHeight();
			int posX = w / 2;
			int posY = h / 2;
			Level _world = null;
			double _x = 0;
			double _y = 0;
			double _z = 0;
			Player entity = Minecraft.getInstance().player;
			if (entity != null) {
				_world = entity.level;
				_x = entity.getX();
				_y = entity.getY();
				_z = entity.getZ();
			}
			Level world = _world;
			double x = _x;
			double y = _y;
			double z = _z;
			RenderSystem.disableDepthTest();
			RenderSystem.depthMask(false);
			RenderSystem.enableBlend();
			RenderSystem.setShader(GameRenderer::getPositionTexShader);
			RenderSystem.blendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA,
					GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ZERO);
			RenderSystem.setShaderColor(1, 1, 1, 1);
			if (ProcAffichageOverlayProcedure.execute(entity)) {
				if (ProcAffichageXpPickaxeProcedure.execute(world))
					Minecraft.getInstance().font.draw(event.getMatrixStack(),
							"" + (int) (SuperpickaxemodModVariables.MapVariables.get(world).XpPickaxe) + "", posX + -162, posY + -112, -13210);
				Minecraft.getInstance().font.draw(event.getMatrixStack(), "XP:", posX + -180, posY + -112, -13210);
				RenderSystem.setShaderTexture(0, new ResourceLocation("superpickaxemod:textures/screens/text_superpickaxeultime.png"));
				Minecraft.getInstance().gui.blit(event.getMatrixStack(), posX + -207, posY + -112, 0, 0, 16, 16, 16, 16);

				if (ProcAffichageUltimeXpProcedure.execute(world))
					Minecraft.getInstance().font.draw(event.getMatrixStack(), "ULTIME", posX + -162, posY + -112, -13210);
				if (ProcAffichageXpPickaxeProcedure.execute(world))
					Minecraft.getInstance().font.draw(event.getMatrixStack(), "/", posX + -135, posY + -112, -13210);
				if (ProcAffichageXpPickaxeProcedure.execute(world))
					Minecraft.getInstance().font.draw(event.getMatrixStack(),
							"" + (int) (SuperpickaxemodModVariables.MapVariables.get(world).XpAObtenir) + "", posX + -126, posY + -112, -13210);
			}
			RenderSystem.depthMask(true);
			RenderSystem.defaultBlendFunc();
			RenderSystem.enableDepthTest();
			RenderSystem.disableBlend();
			RenderSystem.setShaderColor(1, 1, 1, 1);
		}
	}
}
