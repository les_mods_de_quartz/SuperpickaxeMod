
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.mod.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.world.BiomeLoadingEvent;

import net.minecraft.world.level.levelgen.placement.PlacedFeature;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.GenerationStep;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.core.Holder;

import net.mcreator.mod.world.features.ores.SupergoldoreFeature;
import net.mcreator.mod.world.features.ores.SuperemeraudeoreFeature;
import net.mcreator.mod.world.features.ores.SuperdiamsoreFeature;
import net.mcreator.mod.world.features.ores.SupercoaloreFeature;
import net.mcreator.mod.world.features.ores.NetheriteoreFeature;
import net.mcreator.mod.world.features.ores.MineraisdeyelloriteFeature;
import net.mcreator.mod.world.features.ores.MieraisdefersuperstoneFeature;
import net.mcreator.mod.world.features.ores.DarkoreFeature;
import net.mcreator.mod.SuperpickaxemodMod;

import java.util.function.Supplier;
import java.util.Set;
import java.util.List;
import java.util.ArrayList;

@Mod.EventBusSubscriber
public class SuperpickaxemodModFeatures {
	public static final DeferredRegister<Feature<?>> REGISTRY = DeferredRegister.create(ForgeRegistries.FEATURES, SuperpickaxemodMod.MODID);
	private static final List<FeatureRegistration> FEATURE_REGISTRATIONS = new ArrayList<>();
	public static final RegistryObject<Feature<?>> MINERAISDEYELLORITE = register("mineraisdeyellorite", MineraisdeyelloriteFeature::feature,
			new FeatureRegistration(GenerationStep.Decoration.UNDERGROUND_ORES, MineraisdeyelloriteFeature.GENERATE_BIOMES,
					MineraisdeyelloriteFeature::placedFeature));
	public static final RegistryObject<Feature<?>> MIERAISDEFERSUPERSTONE = register("mieraisdefersuperstone", MieraisdefersuperstoneFeature::feature,
			new FeatureRegistration(GenerationStep.Decoration.UNDERGROUND_ORES, MieraisdefersuperstoneFeature.GENERATE_BIOMES,
					MieraisdefersuperstoneFeature::placedFeature));
	public static final RegistryObject<Feature<?>> SUPERCOALORE = register("supercoalore", SupercoaloreFeature::feature, new FeatureRegistration(
			GenerationStep.Decoration.UNDERGROUND_ORES, SupercoaloreFeature.GENERATE_BIOMES, SupercoaloreFeature::placedFeature));
	public static final RegistryObject<Feature<?>> SUPERDIAMSORE = register("superdiamsore", SuperdiamsoreFeature::feature, new FeatureRegistration(
			GenerationStep.Decoration.UNDERGROUND_ORES, SuperdiamsoreFeature.GENERATE_BIOMES, SuperdiamsoreFeature::placedFeature));
	public static final RegistryObject<Feature<?>> SUPEREMERAUDEORE = register("superemeraudeore", SuperemeraudeoreFeature::feature,
			new FeatureRegistration(GenerationStep.Decoration.UNDERGROUND_ORES, SuperemeraudeoreFeature.GENERATE_BIOMES,
					SuperemeraudeoreFeature::placedFeature));
	public static final RegistryObject<Feature<?>> SUPERGOLDORE = register("supergoldore", SupergoldoreFeature::feature, new FeatureRegistration(
			GenerationStep.Decoration.UNDERGROUND_ORES, SupergoldoreFeature.GENERATE_BIOMES, SupergoldoreFeature::placedFeature));
	public static final RegistryObject<Feature<?>> NETHERITEORE = register("netheriteore", NetheriteoreFeature::feature, new FeatureRegistration(
			GenerationStep.Decoration.UNDERGROUND_ORES, NetheriteoreFeature.GENERATE_BIOMES, NetheriteoreFeature::placedFeature));
	public static final RegistryObject<Feature<?>> DARKORE = register("darkore", DarkoreFeature::feature,
			new FeatureRegistration(GenerationStep.Decoration.UNDERGROUND_ORES, DarkoreFeature.GENERATE_BIOMES, DarkoreFeature::placedFeature));

	private static RegistryObject<Feature<?>> register(String registryname, Supplier<Feature<?>> feature, FeatureRegistration featureRegistration) {
		FEATURE_REGISTRATIONS.add(featureRegistration);
		return REGISTRY.register(registryname, feature);
	}

	@SubscribeEvent
	public static void addFeaturesToBiomes(BiomeLoadingEvent event) {
		for (FeatureRegistration registration : FEATURE_REGISTRATIONS) {
			if (registration.biomes() == null || registration.biomes().contains(event.getName()))
				event.getGeneration().getFeatures(registration.stage()).add(registration.placedFeature().get());
		}
	}

	private static record FeatureRegistration(GenerationStep.Decoration stage, Set<ResourceLocation> biomes,
			Supplier<Holder<PlacedFeature>> placedFeature) {
	}
}
