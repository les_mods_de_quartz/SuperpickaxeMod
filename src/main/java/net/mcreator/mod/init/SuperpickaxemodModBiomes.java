
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.mod.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;

import net.minecraft.world.level.biome.Biome;

import net.mcreator.mod.world.biome.CetioplainsBiome;
import net.mcreator.mod.world.biome.CetioforestBiome;
import net.mcreator.mod.SuperpickaxemodMod;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class SuperpickaxemodModBiomes {
	public static final DeferredRegister<Biome> REGISTRY = DeferredRegister.create(ForgeRegistries.BIOMES, SuperpickaxemodMod.MODID);
	public static final RegistryObject<Biome> CETIOFOREST = REGISTRY.register("cetioforest", () -> CetioforestBiome.createBiome());
	public static final RegistryObject<Biome> CETIOPLAINS = REGISTRY.register("cetioplains", () -> CetioplainsBiome.createBiome());

	@SubscribeEvent
	public static void init(FMLCommonSetupEvent event) {
		event.enqueueWork(() -> {
			CetioforestBiome.init();
			CetioplainsBiome.init();
		});
	}
}
