
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.mod.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.world.level.block.Block;

import net.mcreator.mod.block.YelloritefondueBlock;
import net.mcreator.mod.block.UpgradescraftingtableBlock;
import net.mcreator.mod.block.SuperstoneBlock;
import net.mcreator.mod.block.SupergoldoreBlock;
import net.mcreator.mod.block.SuperemeraudeoreBlock;
import net.mcreator.mod.block.SuperdiamsoreBlock;
import net.mcreator.mod.block.SupercobblestoneBlock;
import net.mcreator.mod.block.SupercoaloreBlock;
import net.mcreator.mod.block.PortalcreatorBlock;
import net.mcreator.mod.block.NetheriteoreBlock;
import net.mcreator.mod.block.MineraisdeyelloriteBlock;
import net.mcreator.mod.block.MieraisdefersuperstoneBlock;
import net.mcreator.mod.block.DarkoreBlock;
import net.mcreator.mod.block.CetioforestdirtBlock;
import net.mcreator.mod.block.CetioWoodBlock;
import net.mcreator.mod.block.CetioStairsBlock;
import net.mcreator.mod.block.CetioSlabBlock;
import net.mcreator.mod.block.CetioPressurePlateBlock;
import net.mcreator.mod.block.CetioPlanksBlock;
import net.mcreator.mod.block.CetioLogBlock;
import net.mcreator.mod.block.CetioLeavesBlock;
import net.mcreator.mod.block.CetioForestGrassBlockBlock;
import net.mcreator.mod.block.CetioFenceGateBlock;
import net.mcreator.mod.block.CetioFenceBlock;
import net.mcreator.mod.block.CetioButtonBlock;
import net.mcreator.mod.block.BoutondesortieBlock;
import net.mcreator.mod.block.BlocdeyelloriteBlock;
import net.mcreator.mod.block.AutomineurBlock;
import net.mcreator.mod.SuperpickaxemodMod;

public class SuperpickaxemodModBlocks {
	public static final DeferredRegister<Block> REGISTRY = DeferredRegister.create(ForgeRegistries.BLOCKS, SuperpickaxemodMod.MODID);
	public static final RegistryObject<Block> UPGRADESCRAFTINGTABLE = REGISTRY.register("upgradescraftingtable",
			() -> new UpgradescraftingtableBlock());
	public static final RegistryObject<Block> SUPERSTONE = REGISTRY.register("superstone", () -> new SuperstoneBlock());
	public static final RegistryObject<Block> SUPERCOBBLESTONE = REGISTRY.register("supercobblestone", () -> new SupercobblestoneBlock());
	public static final RegistryObject<Block> MINERAISDEYELLORITE = REGISTRY.register("mineraisdeyellorite", () -> new MineraisdeyelloriteBlock());
	public static final RegistryObject<Block> BLOCDEYELLORITE = REGISTRY.register("blocdeyellorite", () -> new BlocdeyelloriteBlock());
	public static final RegistryObject<Block> YELLORITEFONDUE = REGISTRY.register("yelloritefondue", () -> new YelloritefondueBlock());
	public static final RegistryObject<Block> CETIO_WOOD = REGISTRY.register("cetio_wood", () -> new CetioWoodBlock());
	public static final RegistryObject<Block> CETIO_LOG = REGISTRY.register("cetio_log", () -> new CetioLogBlock());
	public static final RegistryObject<Block> CETIO_PLANKS = REGISTRY.register("cetio_planks", () -> new CetioPlanksBlock());
	public static final RegistryObject<Block> CETIO_LEAVES = REGISTRY.register("cetio_leaves", () -> new CetioLeavesBlock());
	public static final RegistryObject<Block> CETIO_STAIRS = REGISTRY.register("cetio_stairs", () -> new CetioStairsBlock());
	public static final RegistryObject<Block> CETIO_SLAB = REGISTRY.register("cetio_slab", () -> new CetioSlabBlock());
	public static final RegistryObject<Block> CETIO_FENCE = REGISTRY.register("cetio_fence", () -> new CetioFenceBlock());
	public static final RegistryObject<Block> CETIO_FENCE_GATE = REGISTRY.register("cetio_fence_gate", () -> new CetioFenceGateBlock());
	public static final RegistryObject<Block> CETIO_PRESSURE_PLATE = REGISTRY.register("cetio_pressure_plate", () -> new CetioPressurePlateBlock());
	public static final RegistryObject<Block> CETIO_BUTTON = REGISTRY.register("cetio_button", () -> new CetioButtonBlock());
	public static final RegistryObject<Block> CETIOFORESTDIRT = REGISTRY.register("cetioforestdirt", () -> new CetioforestdirtBlock());
	public static final RegistryObject<Block> CETIO_FOREST_GRASS_BLOCK = REGISTRY.register("cetio_forest_grass_block",
			() -> new CetioForestGrassBlockBlock());
	public static final RegistryObject<Block> MIERAISDEFERSUPERSTONE = REGISTRY.register("mieraisdefersuperstone",
			() -> new MieraisdefersuperstoneBlock());
	public static final RegistryObject<Block> SUPERCOALORE = REGISTRY.register("supercoalore", () -> new SupercoaloreBlock());
	public static final RegistryObject<Block> AUTOMINEUR = REGISTRY.register("automineur", () -> new AutomineurBlock());
	public static final RegistryObject<Block> SUPERDIAMSORE = REGISTRY.register("superdiamsore", () -> new SuperdiamsoreBlock());
	public static final RegistryObject<Block> SUPEREMERAUDEORE = REGISTRY.register("superemeraudeore", () -> new SuperemeraudeoreBlock());
	public static final RegistryObject<Block> SUPERGOLDORE = REGISTRY.register("supergoldore", () -> new SupergoldoreBlock());
	public static final RegistryObject<Block> NETHERITEORE = REGISTRY.register("netheriteore", () -> new NetheriteoreBlock());
	public static final RegistryObject<Block> DARKORE = REGISTRY.register("darkore", () -> new DarkoreBlock());
	public static final RegistryObject<Block> PORTALCREATOR = REGISTRY.register("portalcreator", () -> new PortalcreatorBlock());
	public static final RegistryObject<Block> BOUTONDESORTIE = REGISTRY.register("boutondesortie", () -> new BoutondesortieBlock());

	@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD, value = Dist.CLIENT)
	public static class ClientSideHandler {
		@SubscribeEvent
		public static void clientSetup(FMLClientSetupEvent event) {
			AutomineurBlock.registerRenderLayer();
			BoutondesortieBlock.registerRenderLayer();
		}
	}
}
