
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.mod.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.common.ForgeSpawnEggItem;

import net.minecraft.world.level.block.Block;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.BlockItem;

import net.mcreator.mod.item.YelloritefondueItem;
import net.mcreator.mod.item.YelloritedansundeauItem;
import net.mcreator.mod.item.Upgradespeed4Item;
import net.mcreator.mod.item.Upgradespeed3Item;
import net.mcreator.mod.item.Upgradespeed2Item;
import net.mcreator.mod.item.Upgradespeed1Item;
import net.mcreator.mod.item.UpgradeFortune4Item;
import net.mcreator.mod.item.UpgradeFortune3Item;
import net.mcreator.mod.item.UpgradeFortune2Item;
import net.mcreator.mod.item.UpgradeFortune1Item;
import net.mcreator.mod.item.SuperpickaxeUltimeItem;
import net.mcreator.mod.item.Superpickaxe9Item;
import net.mcreator.mod.item.Superpickaxe8Item;
import net.mcreator.mod.item.Superpickaxe7Item;
import net.mcreator.mod.item.Superpickaxe6Item;
import net.mcreator.mod.item.Superpickaxe5Item;
import net.mcreator.mod.item.Superpickaxe4Item;
import net.mcreator.mod.item.Superpickaxe3Item;
import net.mcreator.mod.item.Superpickaxe2Item;
import net.mcreator.mod.item.Superpickaxe1Item;
import net.mcreator.mod.item.SuperdiamantItem;
import net.mcreator.mod.item.SupercoalItem;
import net.mcreator.mod.item.SolideDarkFragmentItem;
import net.mcreator.mod.item.OrcompactItem;
import net.mcreator.mod.item.NetheritecompresseeItem;
import net.mcreator.mod.item.LivredeconnaisancesItem;
import net.mcreator.mod.item.DiamantcompresseItem;
import net.mcreator.mod.item.DarKIngotItem;
import net.mcreator.mod.item.DarKFragmentItem;
import net.mcreator.mod.item.CompresseurItem;
import net.mcreator.mod.SuperpickaxemodMod;

public class SuperpickaxemodModItems {
	public static final DeferredRegister<Item> REGISTRY = DeferredRegister.create(ForgeRegistries.ITEMS, SuperpickaxemodMod.MODID);
	public static final RegistryObject<Item> COMPRESSEUR = REGISTRY.register("compresseur", () -> new CompresseurItem());
	public static final RegistryObject<Item> DIAMANTCOMPRESSE = REGISTRY.register("diamantcompresse", () -> new DiamantcompresseItem());
	public static final RegistryObject<Item> SUPERPICKAXE_1 = REGISTRY.register("superpickaxe_1", () -> new Superpickaxe1Item());
	public static final RegistryObject<Item> SUPERPICKAXE_2 = REGISTRY.register("superpickaxe_2", () -> new Superpickaxe2Item());
	public static final RegistryObject<Item> SUPERDIAMANT = REGISTRY.register("superdiamant", () -> new SuperdiamantItem());
	public static final RegistryObject<Item> SUPERPICKAXE_3 = REGISTRY.register("superpickaxe_3", () -> new Superpickaxe3Item());
	public static final RegistryObject<Item> SUPERPICKAXE_4 = REGISTRY.register("superpickaxe_4", () -> new Superpickaxe4Item());
	public static final RegistryObject<Item> SUPERPICKAXE_5 = REGISTRY.register("superpickaxe_5", () -> new Superpickaxe5Item());
	public static final RegistryObject<Item> SUPERPICKAXE_6 = REGISTRY.register("superpickaxe_6", () -> new Superpickaxe6Item());
	public static final RegistryObject<Item> SUPERPICKAXE_7 = REGISTRY.register("superpickaxe_7", () -> new Superpickaxe7Item());
	public static final RegistryObject<Item> SUPERPICKAXE_8 = REGISTRY.register("superpickaxe_8", () -> new Superpickaxe8Item());
	public static final RegistryObject<Item> SUPERPICKAXE_9 = REGISTRY.register("superpickaxe_9", () -> new Superpickaxe9Item());
	public static final RegistryObject<Item> SUPERPICKAXE_ULTIME = REGISTRY.register("superpickaxe_ultime", () -> new SuperpickaxeUltimeItem());
	public static final RegistryObject<Item> UPGRADESPEED_1 = REGISTRY.register("upgradespeed_1", () -> new Upgradespeed1Item());
	public static final RegistryObject<Item> UPGRADESPEED_2 = REGISTRY.register("upgradespeed_2", () -> new Upgradespeed2Item());
	public static final RegistryObject<Item> UPGRADESPEED_3 = REGISTRY.register("upgradespeed_3", () -> new Upgradespeed3Item());
	public static final RegistryObject<Item> UPGRADESPEED_4 = REGISTRY.register("upgradespeed_4", () -> new Upgradespeed4Item());
	public static final RegistryObject<Item> UPGRADESCRAFTINGTABLE = block(SuperpickaxemodModBlocks.UPGRADESCRAFTINGTABLE,
			SuperpickaxemodModTabs.TAB_SUPERPICKAXEMOD);
	public static final RegistryObject<Item> NETHERITECOMPRESSEE = REGISTRY.register("netheritecompressee", () -> new NetheritecompresseeItem());
	public static final RegistryObject<Item> LIVREDECONNAISANCES = REGISTRY.register("livredeconnaisances", () -> new LivredeconnaisancesItem());
	public static final RegistryObject<Item> SUPERSTONE = block(SuperpickaxemodModBlocks.SUPERSTONE, SuperpickaxemodModTabs.TAB_SUPERPICKAXEMOD);
	public static final RegistryObject<Item> SUPERCOBBLESTONE = block(SuperpickaxemodModBlocks.SUPERCOBBLESTONE,
			SuperpickaxemodModTabs.TAB_SUPERPICKAXEMOD);
	public static final RegistryObject<Item> MINERAISDEYELLORITE = block(SuperpickaxemodModBlocks.MINERAISDEYELLORITE,
			SuperpickaxemodModTabs.TAB_SUPERPICKAXEMOD);
	public static final RegistryObject<Item> BLOCDEYELLORITE = block(SuperpickaxemodModBlocks.BLOCDEYELLORITE,
			SuperpickaxemodModTabs.TAB_SUPERPICKAXEMOD);
	public static final RegistryObject<Item> YELLORITEFONDUE_BUCKET = REGISTRY.register("yelloritefondue_bucket", () -> new YelloritefondueItem());
	public static final RegistryObject<Item> CETIO_WOOD = block(SuperpickaxemodModBlocks.CETIO_WOOD, SuperpickaxemodModTabs.TAB_SUPERPICKAXEMOD);
	public static final RegistryObject<Item> CETIO_LOG = block(SuperpickaxemodModBlocks.CETIO_LOG, SuperpickaxemodModTabs.TAB_SUPERPICKAXEMOD);
	public static final RegistryObject<Item> CETIO_PLANKS = block(SuperpickaxemodModBlocks.CETIO_PLANKS, SuperpickaxemodModTabs.TAB_SUPERPICKAXEMOD);
	public static final RegistryObject<Item> CETIO_LEAVES = block(SuperpickaxemodModBlocks.CETIO_LEAVES, CreativeModeTab.TAB_DECORATIONS);
	public static final RegistryObject<Item> CETIO_STAIRS = block(SuperpickaxemodModBlocks.CETIO_STAIRS, SuperpickaxemodModTabs.TAB_SUPERPICKAXEMOD);
	public static final RegistryObject<Item> CETIO_SLAB = block(SuperpickaxemodModBlocks.CETIO_SLAB, SuperpickaxemodModTabs.TAB_SUPERPICKAXEMOD);
	public static final RegistryObject<Item> CETIO_FENCE = block(SuperpickaxemodModBlocks.CETIO_FENCE, CreativeModeTab.TAB_DECORATIONS);
	public static final RegistryObject<Item> CETIO_FENCE_GATE = block(SuperpickaxemodModBlocks.CETIO_FENCE_GATE, CreativeModeTab.TAB_REDSTONE);
	public static final RegistryObject<Item> CETIO_PRESSURE_PLATE = block(SuperpickaxemodModBlocks.CETIO_PRESSURE_PLATE,
			CreativeModeTab.TAB_REDSTONE);
	public static final RegistryObject<Item> CETIO_BUTTON = block(SuperpickaxemodModBlocks.CETIO_BUTTON, SuperpickaxemodModTabs.TAB_SUPERPICKAXEMOD);
	public static final RegistryObject<Item> CETIOFORESTDIRT = block(SuperpickaxemodModBlocks.CETIOFORESTDIRT,
			SuperpickaxemodModTabs.TAB_SUPERPICKAXEMOD);
	public static final RegistryObject<Item> CETIO_FOREST_GRASS_BLOCK = block(SuperpickaxemodModBlocks.CETIO_FOREST_GRASS_BLOCK,
			SuperpickaxemodModTabs.TAB_SUPERPICKAXEMOD);
	public static final RegistryObject<Item> MIERAISDEFERSUPERSTONE = block(SuperpickaxemodModBlocks.MIERAISDEFERSUPERSTONE,
			SuperpickaxemodModTabs.TAB_SUPERPICKAXEMOD);
	public static final RegistryObject<Item> SUPERCOAL = REGISTRY.register("supercoal", () -> new SupercoalItem());
	public static final RegistryObject<Item> SUPERCOALORE = block(SuperpickaxemodModBlocks.SUPERCOALORE, SuperpickaxemodModTabs.TAB_SUPERPICKAXEMOD);
	public static final RegistryObject<Item> AUTOMINEUR = block(SuperpickaxemodModBlocks.AUTOMINEUR, null);
	public static final RegistryObject<Item> SUPERDIAMSORE = block(SuperpickaxemodModBlocks.SUPERDIAMSORE,
			SuperpickaxemodModTabs.TAB_SUPERPICKAXEMOD);
	public static final RegistryObject<Item> SUPEREMERAUDEORE = block(SuperpickaxemodModBlocks.SUPEREMERAUDEORE,
			SuperpickaxemodModTabs.TAB_SUPERPICKAXEMOD);
	public static final RegistryObject<Item> SUPERGOLDORE = block(SuperpickaxemodModBlocks.SUPERGOLDORE, SuperpickaxemodModTabs.TAB_SUPERPICKAXEMOD);
	public static final RegistryObject<Item> NETHERITEORE = block(SuperpickaxemodModBlocks.NETHERITEORE, SuperpickaxemodModTabs.TAB_SUPERPICKAXEMOD);
	public static final RegistryObject<Item> UPGRADE_FORTUNE_2 = REGISTRY.register("upgrade_fortune_2", () -> new UpgradeFortune2Item());
	public static final RegistryObject<Item> UPGRADE_FORTUNE_3 = REGISTRY.register("upgrade_fortune_3", () -> new UpgradeFortune3Item());
	public static final RegistryObject<Item> UPGRADE_FORTUNE_4 = REGISTRY.register("upgrade_fortune_4", () -> new UpgradeFortune4Item());
	public static final RegistryObject<Item> UPGRADE_FORTUNE_1 = REGISTRY.register("upgrade_fortune_1", () -> new UpgradeFortune1Item());
	public static final RegistryObject<Item> ORCOMPACT = REGISTRY.register("orcompact", () -> new OrcompactItem());
	public static final RegistryObject<Item> YELLORITEDANSUNDEAU = REGISTRY.register("yelloritedansundeau", () -> new YelloritedansundeauItem());
	public static final RegistryObject<Item> DARKORE = block(SuperpickaxemodModBlocks.DARKORE, SuperpickaxemodModTabs.TAB_SUPERPICKAXEMOD);
	public static final RegistryObject<Item> GARDIENDU_DAR_K_ORE = REGISTRY.register("gardiendu_dar_k_ore_spawn_egg",
			() -> new ForgeSpawnEggItem(SuperpickaxemodModEntities.GARDIENDU_DAR_K_ORE, -13434829, -16777216,
					new Item.Properties().tab(SuperpickaxemodModTabs.TAB_SUPERPICKAXEMOD)));
	public static final RegistryObject<Item> DAR_K_FRAGMENT = REGISTRY.register("dar_k_fragment", () -> new DarKFragmentItem());
	public static final RegistryObject<Item> DAR_K_INGOT = REGISTRY.register("dar_k_ingot", () -> new DarKIngotItem());
	public static final RegistryObject<Item> SOLIDE_DARK_FRAGMENT = REGISTRY.register("solide_dark_fragment", () -> new SolideDarkFragmentItem());
	public static final RegistryObject<Item> PORTALCREATOR = block(SuperpickaxemodModBlocks.PORTALCREATOR,
			SuperpickaxemodModTabs.TAB_SUPERPICKAXEMOD);
	public static final RegistryObject<Item> BOUTONDESORTIE = block(SuperpickaxemodModBlocks.BOUTONDESORTIE,
			SuperpickaxemodModTabs.TAB_SUPERPICKAXEMOD);

	private static RegistryObject<Item> block(RegistryObject<Block> block, CreativeModeTab tab) {
		return REGISTRY.register(block.getId().getPath(), () -> new BlockItem(block.get(), new Item.Properties().tab(tab)));
	}
}
