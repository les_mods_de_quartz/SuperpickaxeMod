
package net.mcreator.mod.fluid;

import net.minecraftforge.fluids.ForgeFlowingFluid;
import net.minecraftforge.fluids.FluidAttributes;

import net.minecraft.world.level.material.FluidState;
import net.minecraft.world.level.material.Fluid;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.LiquidBlock;
import net.minecraft.world.item.Rarity;
import net.minecraft.resources.ResourceLocation;

import net.mcreator.mod.init.SuperpickaxemodModItems;
import net.mcreator.mod.init.SuperpickaxemodModFluids;
import net.mcreator.mod.init.SuperpickaxemodModBlocks;

public abstract class YelloritefondueFluid extends ForgeFlowingFluid {
	public static final ForgeFlowingFluid.Properties PROPERTIES = new ForgeFlowingFluid.Properties(SuperpickaxemodModFluids.YELLORITEFONDUE,
			SuperpickaxemodModFluids.FLOWING_YELLORITEFONDUE,
			FluidAttributes
					.builder(new ResourceLocation("superpickaxemod:blocks/textanime_yellorite_fondu"),
							new ResourceLocation("superpickaxemod:blocks/textanime_yellorite_fondu"))

					.rarity(Rarity.RARE))
			.explosionResistance(100f)

			.bucket(SuperpickaxemodModItems.YELLORITEFONDUE_BUCKET).block(() -> (LiquidBlock) SuperpickaxemodModBlocks.YELLORITEFONDUE.get());

	private YelloritefondueFluid() {
		super(PROPERTIES);
	}

	public static class Source extends YelloritefondueFluid {
		public Source() {
			super();
		}

		public int getAmount(FluidState state) {
			return 8;
		}

		public boolean isSource(FluidState state) {
			return true;
		}
	}

	public static class Flowing extends YelloritefondueFluid {
		public Flowing() {
			super();
		}

		protected void createFluidStateDefinition(StateDefinition.Builder<Fluid, FluidState> builder) {
			super.createFluidStateDefinition(builder);
			builder.add(LEVEL);
		}

		public int getAmount(FluidState state) {
			return state.getValue(LEVEL);
		}

		public boolean isSource(FluidState state) {
			return false;
		}
	}
}
