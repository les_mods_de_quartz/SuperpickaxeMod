package net.mcreator.mod.procedures;

import net.minecraft.world.level.LevelAccessor;

import net.mcreator.mod.network.SuperpickaxemodModVariables;

public class Superpickaxe3LorsqueLoutilEstDansLaMainProcedure {
	public static void execute(LevelAccessor world) {
		SuperpickaxemodModVariables.MapVariables.get(world).XpAObtenir = 100;
		SuperpickaxemodModVariables.MapVariables.get(world).syncData(world);
	}
}
