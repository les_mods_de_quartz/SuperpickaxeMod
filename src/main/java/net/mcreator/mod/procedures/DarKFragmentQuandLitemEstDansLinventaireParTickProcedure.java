package net.mcreator.mod.procedures;

import net.minecraftforge.items.CapabilityItemHandler;

import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.Entity;

import net.mcreator.mod.init.SuperpickaxemodModItems;
import net.mcreator.mod.init.SuperpickaxemodModBlocks;

import java.util.concurrent.atomic.AtomicReference;

public class DarKFragmentQuandLitemEstDansLinventaireParTickProcedure {
	public static void execute(Entity entity, ItemStack itemstack) {
		if (entity == null)
			return;
		double nbSupercobble = 0;
		double nbCobbledDeepslate = 0;
		double nbCobblestone = 0;
		if ((new Object() {
			public ItemStack getItemStack(int sltid, ItemStack _isc) {
				AtomicReference<ItemStack> _retval = new AtomicReference<>(ItemStack.EMPTY);
				_isc.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, null).ifPresent(capability -> {
					_retval.set(capability.getStackInSlot(sltid).copy());
				});
				return _retval.get();
			}
		}.getItemStack(8, itemstack)).getItem() == SuperpickaxemodModItems.DAR_K_FRAGMENT.get()) {
			if (entity instanceof Player _playerHasItem
					? _playerHasItem.getInventory().contains(new ItemStack(SuperpickaxemodModBlocks.SUPERCOBBLESTONE.get()))
					: false) {
				nbSupercobble = nbSupercobble + 1;
			} else if (entity instanceof Player _playerHasItem
					? _playerHasItem.getInventory().contains(new ItemStack(Blocks.COBBLED_DEEPSLATE))
					: false) {
				nbCobbledDeepslate = nbSupercobble + 1;
			} else if (entity instanceof Player _playerHasItem ? _playerHasItem.getInventory().contains(new ItemStack(Blocks.COBBLESTONE)) : false) {
				nbCobblestone = nbSupercobble + 1;
			}
		}
	}
}
