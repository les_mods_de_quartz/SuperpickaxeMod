package net.mcreator.mod.procedures;

import net.minecraft.world.level.LevelAccessor;

import net.mcreator.mod.network.SuperpickaxemodModVariables;

public class ProcAffichageXpPickaxeProcedure {
	public static boolean execute(LevelAccessor world) {
		return SuperpickaxemodModVariables.MapVariables.get(world).XpPickaxe != 999999;
	}
}
