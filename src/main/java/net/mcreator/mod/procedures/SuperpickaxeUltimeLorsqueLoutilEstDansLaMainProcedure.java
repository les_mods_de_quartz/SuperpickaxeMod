package net.mcreator.mod.procedures;

import net.minecraft.world.level.LevelAccessor;

import net.mcreator.mod.network.SuperpickaxemodModVariables;

public class SuperpickaxeUltimeLorsqueLoutilEstDansLaMainProcedure {
	public static void execute(LevelAccessor world) {
		SuperpickaxemodModVariables.MapVariables.get(world).XpPickaxe = 999999;
		SuperpickaxemodModVariables.MapVariables.get(world).syncData(world);
	}
}
