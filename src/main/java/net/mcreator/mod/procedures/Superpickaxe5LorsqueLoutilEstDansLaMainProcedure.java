package net.mcreator.mod.procedures;

import net.minecraft.world.level.LevelAccessor;

import net.mcreator.mod.network.SuperpickaxemodModVariables;

public class Superpickaxe5LorsqueLoutilEstDansLaMainProcedure {
	public static void execute(LevelAccessor world) {
		SuperpickaxemodModVariables.MapVariables.get(world).XpAObtenir = 320;
		SuperpickaxemodModVariables.MapVariables.get(world).syncData(world);
	}
}
