package net.mcreator.mod.procedures;

import net.minecraftforge.server.ServerLifecycleHooks;

import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.MobSpawnType;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.Entity;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.MinecraftServer;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.network.chat.ChatType;
import net.minecraft.core.BlockPos;
import net.minecraft.Util;

import net.mcreator.mod.init.SuperpickaxemodModEntities;
import net.mcreator.mod.entity.GardienduDarKOreEntity;

public class DarkoreQuandLeJoueurCommenceADetruireProcedure {
	public static void execute(LevelAccessor world, double x, double y, double z, Entity entity) {
		if (entity == null)
			return;
		if (entity instanceof Player _player && !_player.level.isClientSide())
			_player.displayClientMessage(new TextComponent("..."), (true));
		if (!world.isClientSide()) {
			MinecraftServer _mcserv = ServerLifecycleHooks.getCurrentServer();
			if (_mcserv != null)
				_mcserv.getPlayerList().broadcastMessage(new TextComponent("Comment osez vous d\u00E9truire ce minerais..."), ChatType.SYSTEM,
						Util.NIL_UUID);
		}
		if (!world.isClientSide()) {
			MinecraftServer _mcserv = ServerLifecycleHooks.getCurrentServer();
			if (_mcserv != null)
				_mcserv.getPlayerList().broadcastMessage(new TextComponent((entity.getDisplayName().getString() + " vous devez mourrir...")),
						ChatType.SYSTEM, Util.NIL_UUID);
		}
		world.setBlock(new BlockPos(x + 1, y + 1, z + 1), Blocks.AIR.defaultBlockState(), 3);
		world.setBlock(new BlockPos(x + 1, y + 1, z), Blocks.AIR.defaultBlockState(), 3);
		world.setBlock(new BlockPos(x + 1, y + 1, z - 1), Blocks.AIR.defaultBlockState(), 3);
		world.setBlock(new BlockPos(x, y + 1, z + 1), Blocks.AIR.defaultBlockState(), 3);
		world.setBlock(new BlockPos(x, y + 1, z - 1), Blocks.AIR.defaultBlockState(), 3);
		world.setBlock(new BlockPos(x, y + 1, z + 1), Blocks.AIR.defaultBlockState(), 3);
		world.setBlock(new BlockPos(x - 1, y + 1, z - 1), Blocks.AIR.defaultBlockState(), 3);
		world.setBlock(new BlockPos(x - 1, y + 1, z + 1), Blocks.AIR.defaultBlockState(), 3);
		world.setBlock(new BlockPos(x - 1, y + 1, z), Blocks.AIR.defaultBlockState(), 3);
		world.setBlock(new BlockPos(x + 1, y + 2, z + 1), Blocks.AIR.defaultBlockState(), 3);
		world.setBlock(new BlockPos(x + 1, y + 2, z), Blocks.AIR.defaultBlockState(), 3);
		world.setBlock(new BlockPos(x + 1, y + 2, z - 1), Blocks.AIR.defaultBlockState(), 3);
		world.setBlock(new BlockPos(x, y + 2, z + 1), Blocks.AIR.defaultBlockState(), 3);
		world.setBlock(new BlockPos(x, y + 2, z - 1), Blocks.AIR.defaultBlockState(), 3);
		world.setBlock(new BlockPos(x, y + 2, z + 1), Blocks.AIR.defaultBlockState(), 3);
		world.setBlock(new BlockPos(x - 1, y + 2, z - 1), Blocks.AIR.defaultBlockState(), 3);
		world.setBlock(new BlockPos(x - 1, y + 2, z + 1), Blocks.AIR.defaultBlockState(), 3);
		world.setBlock(new BlockPos(x - 1, y + 2, z), Blocks.AIR.defaultBlockState(), 3);
		if (world instanceof ServerLevel _level) {
			Entity entityToSpawn = new GardienduDarKOreEntity(SuperpickaxemodModEntities.GARDIENDU_DAR_K_ORE.get(), _level);
			entityToSpawn.moveTo(x, (y + 1), z, world.getRandom().nextFloat() * 360F, 0);
			if (entityToSpawn instanceof Mob _mobToSpawn)
				_mobToSpawn.finalizeSpawn(_level, world.getCurrentDifficultyAt(entityToSpawn.blockPosition()), MobSpawnType.MOB_SUMMONED, null, null);
			world.addFreshEntity(entityToSpawn);
		}
	}
}
