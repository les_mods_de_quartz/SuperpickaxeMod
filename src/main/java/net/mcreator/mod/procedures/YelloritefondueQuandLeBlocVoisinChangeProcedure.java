package net.mcreator.mod.procedures;

import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.core.BlockPos;

import net.mcreator.mod.init.SuperpickaxemodModBlocks;

public class YelloritefondueQuandLeBlocVoisinChangeProcedure {
	public static void execute(LevelAccessor world, double x, double y, double z) {
		if (((world.getFluidState(new BlockPos(x + 1, y, z)).createLegacyBlock()).getBlock() == Blocks.WATER
				|| (world.getFluidState(new BlockPos(x - 1, y, z)).createLegacyBlock()).getBlock() == Blocks.WATER
				|| (world.getFluidState(new BlockPos(x, y, z + 1)).createLegacyBlock()).getBlock() == Blocks.WATER
				|| (world.getFluidState(new BlockPos(x, y, z - 1)).createLegacyBlock()).getBlock() == Blocks.WATER)
				&& (world.getFluidState(new BlockPos(x, y, z)).createLegacyBlock()).getBlock() == SuperpickaxemodModBlocks.YELLORITEFONDUE.get()) {
			world.setBlock(new BlockPos(x, y, z), SuperpickaxemodModBlocks.BLOCDEYELLORITE.get().defaultBlockState(), 3);
		}
	}
}
